import Vuex from 'vuex'
import { newsFeedRes } from '~/assets/api/newsfeed.json'
import { navFeedRes } from '~/assets/api/navfeed.json'
import { showsFeed } from '~/assets/api/showsfeed.json'
const vuexStore = () => {
  return new Vuex.Store({
    state: {
      counter: 0,
      user: {},
      userIp: null,
      userBadgeOpen: false,
      mobileMenuOpen: false,
      triviaMessage: '',
      categoryPresets: ['reviews', 'technology', 'weekly', 'corporate', 'health', 'lifestyle', 'win'],
      newsFeed: newsFeedRes,
      navFeed: navFeedRes,
      showsFeed: showsFeed,
      userStatus: {
        currentStatus: 0,
        statusTypes: [
          { type: 'Online', id: 0, color: 'green' },
          { type: 'Away', id: 1, color: 'orange' },
          { type: 'Busy', id: 2, color: 'red' },
          { type: 'Invisible', id: 3, color: 'magenta' },
          { type: 'Offline', id: 4, color: 'grey' }
        ]
      },
      notifications: [{
        title: 'Message',
        desc: 'This is a message notification type.',
        type: 'message-circle',
        color: 'grey',
        seen: false
      },
      {
        title: 'Fancy',
        desc: 'This is super fancy notification message.',
        type: 'trending-up',
        color: 'magenta',
        seen: false
      },
      {
        title: 'Success',
        desc: 'This is success notification message.',
        type: 'check-circle',
        color: 'green',
        seen: false
      },
      {
        title: 'Info',
        desc: 'This is an info notification message.',
        type: 'bell',
        color: 'blue',
        seen: false
      },
      {
        title: 'Warning',
        desc: 'This is a warning notification message.',
        type: 'alert-triangle',
        color: 'orange',
        seen: false
      },
      {
        title: 'Error',
        desc: 'This is an error notification message.',
        type: 'alert-octagon',
        color: 'red',
        seen: false
      }]
    },
    mutations: {
      SET_USER (state, user) {
        state.user = user || null
      },
      SET_IP (state, ip) {
        state.userIp = ip || null
      },
      SET_PAGE_TITLE (state, name) {
        state.pageTitle = name
      },
      SET_MOBILE_MENU (state) {
        state.mobileMenuOpen = !state.mobileMenuOpen
      },
      SET_USER_BADGE (state) {
        state.userBadgeOpen = !state.userBadgeOpen
      },
      SET_USER_STATUS (state, val) {
        state.userStatus.currentStatus = parseInt(val)
      },
      SET_TRIVIA_MESSAGE (state, payload) {
        state.triviaMessage = payload
      }
    },
    getters: {
      isAuthenticated (state) {
        return !!state.user
      },
      loggedUser (state) {
        return state.user
      },
      notifications (state) {
        return state.notifications.filter(item => !item.seen)
      },
      userStatus (state) {
        return state.userStatus
      },
      getUserIp (state, actions) {
        return state.userIp
      },
      getNavFeed (state) {
        return state.navFeed
      }
    },
    actions: {
      async nuxtServerInit ({ commit, app }) {
        const message = await app.$axios.$get('http://numbersapi.com/random?json')
        const ip = await app.$axios.$get('https://icanhazip.com')
        commit('SET_IP', ip)
        commit('SET_TRIVIA_MESSAGE', message)
      }
    }
  })
}

export default vuexStore
