import test from 'ava'
import express from 'express'
import { Nuxt, Builder } from 'nuxt'
import { resolve } from 'path'

const port = 4000
let nuxt = null
let server = null

test.before('Init Nuxt.js', async t => {
  const app = express()
  const rootDir = resolve(__dirname, '../../')
  let config = {}

  try {
    config = require(resolve(rootDir, 'nuxt.config.js'))
  } catch (e) {
    console.log('Unable to locate nuxt config!')
  }

  config.rootDir = rootDir
  config.dev = false

  nuxt = new Nuxt(config)

  await new Builder(nuxt).build()

  app.use(nuxt.render)

  server = app.listen(port)
})

test('Route / exits and render HTML', async t => {
  let context = {}
  const { html } = await nuxt.renderRoute('/temp', context)
  t.true(html.includes('<h1 class="red">Hello world!</h1>'))
})

test('Route / exits and render HTML with CSS applied', async t => {
  const window = await nuxt.renderAndGetWindow('http://localhost:4000/')
  const element = window.document.querySelector('.red')
  t.not(element, null)
  t.is(element.textContent, 'Hello world!')
  t.is(element.className, 'red')
  t.is(window.getComputedStyle(element).color, 'red')
})

test.after('Closing server and nuxt.js', t => {
  server.close()
  nuxt.close()
})
