export default function ({ store, redirect }) {
  if (!store.getters.isAuthenticated) {
    let ref = store.$router.history.current.path || '/'
    return redirect('/auth/sign-in?ref=' + ref)
  }
}
