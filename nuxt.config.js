const app = {
  mode: 'spa', // set this to 'spa' (single page app) or 'universal' for ssr support
  pageTitle: 'AKMODAN 🚀',
  pageDescription: 'AKMODAN II 🚀 Legacy',
  fonts: 'Roboto+Slab:300|Open+Sans:400',
  fontsApi: 'https://fonts.googleapis.com/css?family=',
  middleware: ['check-auth'],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/sitemap',
    '@nuxtjs/component-cache'
  ],
  plugins: [
    { src: '~/plugins/vue-feather-icons.js', ssr: true },
    { src: '~/plugins/vue-lazyload.js', ssr: false },
    { src: '~/plugins/vue-click-outside.js', ssr: false },
    { src: '~/plugins/vue-notification.js', ssr: false },
    { src: '~/plugins/vue-mq.js', ssr: false }
  ],
  script: [
    // examples:
    // { src: '/js/defer.js', defer: '' },
    // { src: '/js/polyfils.js' }
  ],
  vendor: [], // use in conjunction with 'universal' mode.
  favicon: '/img/favicon.ico',
  css: ['~/assets/scss/all.scss'],
  styleResources: {
    // scss variables injection
    scss: ['./assets/scss/utils/_all.scss', './assets/scss/vars/_all.scss']
  },
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    prod: process.env.PRODUCTION || false,
    auth0: {
      'AUTH0_CLIENT_ID': process.env.AUTH0_CLIENT_ID || 'AUTH0_CLIENT_ID',
      'AUTH0_CLIENT_DOMAIN': process.env.AUTH0_CLIENT_DOMAIN || 'AUTH0_CLIENT_DOMAIN',
    }
  },
  loading: {
    color: '#CE00BC',
    height: '2px'
  },
  routes () {
    // TODO: fix hardcoded news categories
    let routes = [],
        newsCategories = ['reviews', 'technology', 'weekly', 'corporate', 'health', 'lifestyle', 'win'],
        showCategories = ['Action','Adventure','Anime','Comedy','Crime','Drama','Family','History','Horror','Music','Science-Fiction','Supernatural','Thriller','War']

    // statically generate news category pages i.e.: /news/reviews
    newsCategories.forEach(item => routes.push({route: '/news/' + item}))
    showCategories.forEach(item => routes.push({route: '/shows/' + item.toLowerCase()}))
    
    return routes;
  },
  distFolder: 'dist',
}

module.exports = {
  mode: app.mode,
  env: app.env,
  // HTML head
  head: {
    title: app.pageTitle,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0' },
      { hid: 'description', name: 'description', content: app.pageDescription }
    ],
    noscript: [
      { innerHTML: 'Body No Scripts', body: true }
    ],
    // static content goes here
    link: [
      { rel: 'icon', type: 'image/x-icon', href: app.favicon },
      { rel: 'stylesheet', href: app.fontsApi + app.fonts }
    ],
    script: app.script
  },
  css: app.css,
  loading: app.loading,
  router: {
    // TODO: find a good way to use this
    // extendRoutes (routes, resolve) {
    //   routes.push({
    //     name: 'custom',
    //     path: '*',
    //     component: resolve(__dirname, 'pages/index.vue')
    //   })
    // },
    middleware: app.middleware
  },
  //axios config
  axios: {
    BaseURL: app.env.baseUrl,
    browserBaseURL: app.env.baseUrl
  },
  modules: app.modules,
  plugins: app.plugins,
  build: {
    vendor: app.vendor,
    styleResources: {
      scss: app.styleResources.scss
    },
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    dir: app.dir,
    interval: 100,
    minify: {
      removeComments: true,
      sortClassName: true
    },
    routes: app.routes
  },
  sitemap: {
   path: '/sitemap.xml',
   hostname: app.env.baseUrl,
   cacheTime: 1000 * 60 * 60,
   generate: true, // Enable me when using nuxt generate
   exclude: [
     '/users',
     '/users/**',
     '/auth/**'
   ]
  }
}
