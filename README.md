# Akmodan II
> Akmodan II - Legacy

> Demo: https://akmodan2.netlify.com


## Requirements
- Git (https://git-scm.com/)
- Nodejs (https://nodejs.org)

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

## Screenshots
![akmodan](https://akmodan2.netlify.com/img/akmodan.svg)

![akmodan](https://s18.postimg.org/qpq21blm1/localhost_3000_Laptop_with_MDPI_screen.png)

![akmodan](https://s18.postimg.org/o8eau29fd/localhost_3000_Laptop_with_MDPI_screen_2.png)

![akmodan](https://s18.postimg.org/a1yjyu6a1/localhost_3000_Laptop_with_MDPI_screen_3.png)

![akmodan](https://s18.postimg.org/nvmwnv409/localhost_3000_Laptop_with_MDPI_screen_4.png)
