import Vue from 'vue'
import VueMq from 'vue-mq'

Vue.use(VueMq, {
  breakpoints: {
    mobile: 321,
    'mobile-l': 569,
    tablet: 769,
    'tablet-l': 1025,
    desktop: 1026
  }
})
