import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload)

// or with options
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '~/static/img/error.png',
  loading: '~/static/img/loader.svg',
  attempt: 3,
  lazyComponent: true,
  observer: true,
  observerOptions: {
    rootMargin: '0px',
    threshold: 0.1
  },
  silent: true
  // cdn
  // filter: {
  //   progressive (listener, options) {
  //       const isCDN = /qiniudn.com/
  //       if (isCDN.test(listener.src)) {
  //         listener.el.setAttribute('lazy-progressive', 'true')
  //         listener.loading = listener.src + '?imageView2/1/w/10/h/10'
  //       }
  //   },
  //   webp (listener, options) {
  //       if (!options.supportWebp) return
  //       const isCDN = /qiniudn.com/
  //       if (isCDN.test(listener.src)) {
  //         listener.src += '?imageView2/2/format/webp'
  //       }
  //   }
  // }
})
