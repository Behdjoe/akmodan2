import { setSecret } from './auth'
import uuid from 'uuid'

const getLock = (options) => {
  let config = {}
  if (process.env.prod) {
    config = process.env.auth0
  } else {
    config = require('~/config.json')
  }
  const Auth0Lock = require('auth0-lock').default
  return new Auth0Lock(config.AUTH0_CLIENT_ID, config.AUTH0_CLIENT_DOMAIN, options)
}

const getBaseUrl = () => `${window.location.protocol}//${window.location.host}`
const getReferrer = () => {
  return window.location.search.split('?ref=')[1] || '/'
}

const getOptions = (container) => {
  const secret = uuid.v4()
  setSecret(secret)
  return {
    container,
    closable: true,
    auth: {
      responseType: 'token',
      redirectUrl: `${getBaseUrl()}/auth/signed-in?ref=${getReferrer()}`,
      params: {
        scope: 'openid profile email',
        state: secret
      }
    }
  }
}

export const show = (container) => getLock(getOptions(container)).show()
export const logout = () => getLock().logout({ returnTo: getBaseUrl() })
