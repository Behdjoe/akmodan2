import Vue from 'vue'
// import {VueFeatherIconsSsr as icon} from 'vue-feather-icons-ssr'
// use the code below to support SSR
import {VueFeatherIconsSsrJsx as icon} from 'vue-feather-icons-ssr'

Vue.component('icon', icon)
